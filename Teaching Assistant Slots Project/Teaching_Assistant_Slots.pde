// We control which screen is active by settings / updating
// gameScreen variable. We display the correct screen according
// to the value of this variable.
// 0: Initial Screen/tital card
// 1: Title Card
// 2: Game Screen
// 3: Zach (win Screen)
// 4: johnathon (win screen)
// 5: linux (win screen)

import ddf.minim.*;
import java.util.Arrays;


//VARIABLES//

float c;

int gameScreen = 0;

PImage slot;
PImage head;
PImage zack;
PImage jonathan;
PImage keziahead;
PImage kristinahead;

float x,y;
float theta = 0;
PFont font;
int counter = 0;

boolean bool_pressedonlever; //when you are pressing the mouse on the lever
boolean bool_onlever; //when the mouse is on the lever
boolean bool_spin; //the lever has been pulled
boolean bool_spinning; //the reels are spinning
boolean win; //win test to demonstrate stories

float float_spinframe; //to calculate how fast to switch the images
int spinframe;
int time;

boolean bool_reels; //to display the reels after it has spun
int reel1; //the values that correspond to each image for each reel
int reel2;
int reel3;

PImage[] faces = new PImage[4]; //new array to show TA's faces
PImage[] middle = new PImage[4]; //new array to show middle frame of TA's faces

//Sound Files: need to implement other stories recorded

//create sound file player
Minim minim;
//AudioPlayer player;
AudioPlayer intro;
AudioPlayer zach;
AudioPlayer john;
AudioPlayer kezia;
AudioPlayer kristina;


AudioInput input;

//setup including files for all TAs
void setup() {
  size(1000,1000);
  smooth();
  slot = loadImage("slot.png");
  //head = loadImage("head.jpg");
  zack = loadImage("zack.jpg");
  jonathan = loadImage("jonathan.jpg");
  keziahead = loadImage("kezia.jpg");
  kristinahead = loadImage("kristina.jpg");
  loadImages();
  
  //where to find sound files
  minim = new Minim(this);
  intro = minim.loadFile("intro.wav");
  zach = minim.loadFile("zach.wav");
  john = minim.loadFile("jonathan.wav");
  kezia = minim.loadFile("kezia.wav");
  kristina = minim.loadFile("kristina.wav");
}

void draw() {
  if(gameScreen == 0) {
    initScreen(); 
  
  } if(gameScreen == 1) {
    introScreen();
  
  } else if (gameScreen == 2) {
    gameScreen();
/*  
  } else if (gameScreen == 3) {
    zackScreen();
  
  } else if (gameScreen == 4) {
    johnScreen();
  
  } else if (gameScreen == 5) {
    kaziaScreen();
  
  } else if (gameScreen == 6) {
    katScreen();
    
  } else if (gameScreen == 6) {
    restartScreen(); */
  } 
}

/***********Initiation Factors*********/

void initScreen() {
  drawinit();
}

void introScreen() {
  drawintro();
}
 
 
void gameScreen() {
  drawgame();
}

void zackScreen() {
  drawzack();
}

void johnScreen() {
  drawjohn();
}

void keziaScreen() {
  drawkezia();
}

void katScreen() {
  drawkristina();
}

public void keyPressed() { //Press enter to skip intro/cutscene
  if (key == ENTER) {
    if (gameScreen==0) { 
    gameScreen=1;
  } 
  else if (gameScreen==1){
    gameScreen=2;
    intro.close();
  }
  }
}
public void mousePressed() {
  // if we are on the initial screen, click to play intro/cutscene/game
  if (gameScreen==0) { 
    gameScreen=1;
  } 
  else if (gameScreen==1) {
    gameScreen=2;
    intro.close();
  }
  
  if (bool_onlever && !bool_spinning) {
    bool_pressedonlever = true; //you can't pull the lever if it is already spinning. and you have to be on the lever
  }
  if (mouseButton == RIGHT) { //Pull lever using right mouse button
    win = true;
  } else {
    win = false;
  }
}

void mouseReleased() { //when mouse released, spin reels
  if (bool_pressedonlever) { 
    bool_pressedonlever = false; 
    if (!bool_spin && !bool_spinning && mouseY > 100) { //you have to pull the lever down
      bool_reels = false; 
      bool_spin = true; 
    }
  }
}




/*********SCREEN CONTENTS*************/


void drawinit() { //draw intro slide; spinning heads
  background(255);
  textAlign(CENTER);
  fill(52, 73, 94);
  textSize(70);
  text("Story Slots", 500, 250);
  stroke(0); 
  imageMode(CENTER);
  image(slot, 500, 500);
  
  // Translate to center of window
  
  translate(width/2, height/2);

  // The coins/faces rotate around the slotmachine

  pushMatrix();
  rotate(theta*1);
  translate(200, 0);
  image(zack,0,0);
  zack.resize(75,0);
  popMatrix();
  

  theta += 0.01;


  pushMatrix();
  rotate(theta*1.05);
  translate(200, 0);
  image(jonathan,0,0);
  jonathan.resize(75,0);
  popMatrix();
  

  theta += 0.01;

  pushMatrix();
  rotate(theta*1.10);
  translate(200, 0);
  image(keziahead,0,0);
  keziahead.resize(75,0);
  popMatrix();
  

  theta += 0.01;

  pushMatrix();
  rotate(theta*1.15);
  translate(200, 0);
  image(kristinahead,0,0);
  kristinahead.resize(75,0);
  popMatrix();
  

  theta += 0.01;

}


/********FUNCTIONS**********/

void drawintro() { //simple intro cutscene
  background(0);
  fill(255);
  intro.play();
  String text1 = "In the very distant future... The year 2020. Money has become worthless. The only currency left... Are Stories.";
  if(counter < text1.length()) {
    if (frameCount % 4 == 0) 
      counter++;
  }
    
  text(text1.substring(0, counter), 0, 40, width, height);
}
  
void drawgame() { //function that draws and deploys slot machine
  bool_onlever = mouseX > 830 && mouseX < 870 && mouseY > 15 && mouseY < 250; //when the mouse will be on the lever
  text("Story Slots", 200, 25);
  strokeWeight(1);
  //size(1040,680);
  background(255,210,0);
  colorMode(HSB);
  
  //rainbow background!
  if (c >= 255)  c=0;  else  c++;
  background(c, 255, 255);
  
  //the gray background on top of the rainbow
  fill(50,50,50);
  rect(20,20,810,640);
  
  //three evenly spaced rectangles for the slot images
  fill(255);
  rect(55,55,240,290);
  fill(255);
  rect(305,55,240,290);
  fill(255);
  rect(555,55,240,290); 

if (bool_pressedonlever) { //if lever is pressed 
    if (mouseY > 50 && mouseY <150) { //cordinates for lever reactoin
      leveron(); 
    } else if (mouseY <= 50) {
      leverup(); 
    } else {
      leverdown(); 
    }
  } else {
    leverup(); 
  }


if (bool_spin) { // if lever pulled
    bool_spinning = true; 
    bool_spin = false;

    reel1 = int(random(4)); //random values to show the different images
    reel2 = int(random(4));
    reel3 = int(random(4));

    if (win) { //sets reels to = winning state
      reel2 = reel1;
      reel3 = reel1;
    }
  }

  imageMode(CORNER); // Centers our images on the corner of the rectangle
  
  if (bool_spinning) { //Sets all reels, frames them in postiton, and spins them using boolion spin
    if (time < 120) { 
      image(middle[spinframe], 56, 56, 240, 290); 
      image(middle[spinframe], 306, 56, 240, 290);  
      image(middle[spinframe], 556, 56, 240, 290);
    }else if (time < 240) { // at 4 seconds
      image(faces[reel1], 56, 56, 240, 290); 
      image(middle[spinframe], 306, 56,240, 290); 
      image(middle[spinframe], 556, 56, 240, 290);
    } else if (time < 360) { //at 6 seconds
      image(faces[reel1], 56, 56, 240, 290); 
      image(faces[reel2], 306, 56, 240, 290);
      image(middle[spinframe], 556, 56, 240, 290);
    } else if (time < 370) {
      image(faces[reel1], 56, 56, 240, 290); 
      image(faces[reel2], 306, 56, 240, 290);
      image(faces[reel3], 556, 56, 240, 290);
    } else {
      time = 0; //reset condition
      bool_spinning = false; 
      bool_reels = true; 
    }
    float_spinframe += 0.2; 
    if (float_spinframe > 4) {
      float_spinframe = 0;
    }
    spinframe = floor(float_spinframe);    
    time ++; 
  }

  if (bool_reels) { //shows all the reels, sets win conditions
    image(faces[reel1], 56, 56, 240, 290);
    image(faces[reel2], 306, 56, 240, 290);
    image(faces[reel3], 556, 56, 240, 290);
    if (reel1 == reel2 && reel1 == reel3) {
      //System.out.println(reel1+1); //test for which image in which frame
      if(reel1+1 == 1) {
        //win condition for zack
      drawzack();
    }
      if (reel1+1 == 2) {
        //win condition for jonathan
        drawjohn();
      }
      if(reel1+1 == 3) {
        drawkezia();
      }
      if(reel1+1 == 4) {
        drawkristina();
      }
  }
}
}

/********LEVER*******/

void leverup() {
  strokeWeight(20);
  stroke(150);
  line(850, 100, 850, 20);
  strokeWeight(1);
  noStroke();
  ellipse(850, 20, 25, 25);
}

void leverdown() {
  strokeWeight(20);
  stroke(150);
  line(850, 100, 850, 175); 
  strokeWeight(1);
  noStroke();
  ellipse(850, 175, 25, 25);
}

void leveron() {
  strokeWeight(20);
  stroke(150);
  line(850, 100, 850, mouseY);
  strokeWeight(1);
  noStroke();
  ellipse(850, mouseY, 25, 25);
}

void loadImages() { //load all of our images
  
  faces[0] = loadImage("1.png");
  faces[1] = loadImage("2.png");
  faces[2] = loadImage("3.png");
  faces[3] = loadImage("4.png");

  middle[0] = loadImage("spin1.png");
  middle[1] = loadImage("2.png");
  middle[2] = loadImage("3.png");
  middle[3] = loadImage("4.png");
}

/************WIN SCREENS/STORIES**********/

void drawzack() { //draws zach's story
  background(0);
  fill(255);
  zach.play();
  String text2 = "In the distant future, Approximatly 5 days from now... or a year, look I'm a storyteller, not a fortune teller." 
  +"ANYWAY, once upon a time, Zachary went on a fishing trip to find a mysterious cure for his lactose intolerence. While on the boat, he asked the sailor:"
  +"Do you know Calculus(Zack himself knowing nothing of Calculus), Biology, Zoology, Geography, or anything about the greatest programmer in history…" 
  +"the magnificent Dr.Gardner?"
  +"The Sailor answer no to Calculus, he then shook his head answering to no Programming, Biology, Zoology, Geography."
  +" "
  +"Looking puzzled and perplexed he looked Zachary directly in the eyes and said Dr.Gardner is legendary around these parts, how dare you question my intelligence."
  +"After a while on the boat started to sink. The sailor asked Zachary, do you know swimology & escapology from sharkology?"
  +"Zachary while shaking in his boots answers “No, I studied Computer Science!”"
  +"The sailor responders with: “Well, sharkology & crocodilogy will eat your assology, headology & you will dieology because of your mouthology”";
  
  if(counter < text2.length()) { //creates a typewriter effect, counting through each letter
    if (frameCount%3==0)  
      counter++;
  }
    
  text(text2.substring(0, counter), 0, 40, width, height);
  textSize(25);
}

void drawjohn() { //draw jonathan story
   background(0);
  fill(255,255,255);
  john.play();
  String text3 = "Years after his graduation, Johnathan loses interest in Computer Science and for some odd reason falls in love with being a captain of a navy ship."
  +"With his degree in hand, he walks into a navy recruiter's office and with his intelligence and outstanding beard he instantly earns the rank of Captain for the USS, Panther (After his favorite football team). And will now be addressed as Captain McGill."
  +"One day, he was alerted by his 1st mate that there is a pirate ship coming towards his position. He tells the sailor to get him his red shirt!"
  +"The sailor asked: “Captain, Why do you need a red shirt?”"
  +"Captain McGill replies, “So that when I bleed, you guys don't notice and aren't discouraged.”"
  +"They are victorious on this day, Days pass but on the 5th day”"
  +"Captain McGill is alerted of pirates once again, and once again requesting his redshirt."
  +"Seconds later*"
  +"Sailor: “Sir, this time there are about 50 pirate ships that are coming towards our boat.”"
  +"Captain McGill yells, “Bring me my brown pants!”";
    
  if (counter < text3.length()) {
    if (frameCount%3==0)
    counter++;
  
  }
  
  text(text3.substring(0, counter), 0, 40, width, height);
  textSize(25);
}

void drawkezia() { //draw kezia story
  background(25);
  fill(255,255,255);
  kezia.play();
  String text4 = "One day there was a beautiful princess named Kezia. Her reign was over linuxville and all its peasants and dorks."
+"Everyone in Linuxville respected and loved the princess because of her keen presence and her brilliance with technology."
+"In this land her brother and her are the only one who knows how to use technology."
+"The peasants were not all unfamiliar with it some knew how to call on a touch “mobile phone” some could even text a sentence without it taking 45 hours!"
+"Unfortunately, Kezia and her brother had informed them for the betterment of the land."
+"This got very nerve wracking to the point where Princess Kezia gave a final order to the peasants of Linuxville."
+"She stated “GOOGLE IT, DON’T ASK ME NO MORE!!” "
+"The people of Linuxville  were stunned in confusion and whispered “whats google?” ”";
  if (counter < text4.length()) {
    if (frameCount%3==0)
    counter++;
  
  }
  
  text(text4.substring(0, counter), 0, 40, width, height);
  textSize(25);
}
 
void drawkristina() { //draw kirstina story
  background(25);
  fill(255,255,255);
  kristina.play();
  String text5 = "One day, a girl named Kristina, was walking down a road when a frog called to her, “Girl, if you kiss me, I will turn into a beautiful prince.”"
 + "The boy picked up the frog, smiled at it, then placed the frog into his pocket. A few minutes later, the frog said, “Girl, if you kiss me and turn me back"
 + "into a handsome prince, and I will stay with you for a week.” The girl took the frog from his pocket, smiled at it, then put it back into his pocket."
 +"A few minutes later, the frog said, “Girl, if you kiss me and turn me back into a handsome prince, I will do ANYTHING you want!”"
 +"The girl took the frog from his pocket, smiled, and put it back. Finally, the frog cried, “Kristina, what is the matter, I have told you that I am a handsome prince,"
 +"and if you kiss me, I will stay with you and do ANYTHING you want!”"
 +"The girl took the frog from her pocket and said, “Look, I am an engineering student, I have no time for a boyfriend, but a talking frog is cool!”";
if (counter < text5.length()) {
    if (frameCount%3==0)
    counter++;
  
  }
  
  text(text5.substring(0, counter), 0, 40, width, height);
  textSize(25);
}
