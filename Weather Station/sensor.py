#!/usr/bin/env python3  
  
import os  
import time  
import datetime  
import glob 
import pymysql
pymysql.install_as_MySQLdb() 
import MySQLdb  
from time import strftime  
#import Adafruit_DHT  
#from board import 4
from pytz import timezone

import board
import adafruit_dht
import socket
from datetime import datetime
dhtDevice = adafruit_dht.DHT22(board.D4)  
  
#Variables for MySQL  
db = MySQLdb.connect(host='192.168.1.129', port = 3306,  user='jordan', read_timeout = 60, passwd='admin', db='sensor') # replace password with your password  
cur = db.cursor()  
  
def dateTime(): #convert to y/m/d/h/m/s  

	localtime = timezone('US/Central')
	now = datetime.now(localtime)
	secs = now.strftime("%Y-%m-%dT%H:%M:%S")

	#secs = float(time.time())  
        #secs = secs*1000  
	return secs  

def currentSensor():
	hostname = socket.gethostname()
	return hostname

def tempRead(): #read temperature, return float with 3 decimal places  
	
	temperature_C = dhtDevice.temperature
	temperature_F = temperature_C * (9 / 5) + 32

	degrees = float('{0:.3f}'.format(temperature_F))
	
	return degrees
	streamer.log(SENSOR_LOCATION_NAME + " Temperature(F)", degrees)

  
def humidityRead(): #read humidity, return float with 3 decimal places  
        humidity = float('{0:.3f}'.format(dhtDevice.humidity))  
        return humidity  
  
secs = dateTime()  
temperature = tempRead()    
humidity = humidityRead()  
hostname = currentSensor()

sql = ("""INSERT INTO temp (sensor, timestamp, temperature, humidity) VALUES (%s,%s,%s,%s)""", (hostname, secs, temperature, humidity))  
  
try:  
    print ("Writing to the database...")  
    cur.execute(*sql)  
    db.commit()  
    print ("Write complete")  
  
except:  
    db.rollback()  
    print ("We have a problem")  
  
cur.close()  
db.close()  

print (hostname)  
print (secs)  
print (temperature)    
print (humidity)  
