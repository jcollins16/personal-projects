import com.birdbraintechnologies.Finch;


public class Dance
{

   public static void main(final String[] args)
   {
	  // Instantiating the Finch object
      Finch myFinch = new Finch();

      
	myFinch.playClip("C:/Users/Jordan/Desktop/FinchJavaCommandLine/src/Code/candyGirl/CandyGirl.wav");
	

//DANCE FINCH DANCE!
for(int h = 0; h < 2; h++){
myFinch.sleep(600);
verse();
chorus();
myFinch.sleep(600);
verse();
bridge();
}
bridge();
verse();
bridge();
chorus();
chorus();
chorus();

//finishing and turning it off
myFinch.quit();
System.exit(0);
}

public static void bridge(){

//Initializing needed info
Finch myFinch = new Finch();
int time = 400;

for(int i = 0; i < 2; i++){
//LED to red, wag "tail"
myFinch.setLED(255,0,0);
myFinch.setWheelVelocities(-255,255,time);
myFinch.setWheelVelocities(255,-255,time);

//Pause
myFinch.sleep(2 / time);

//LED to blue, wag "tail"
myFinch.setLED(0,0,255);
myFinch.setWheelVelocities(-255,255,time);
myFinch.setWheelVelocities(255,-255,time);
}
//Pause
myFinch.sleep(2 * time);
for(int i = 0; i < 2; i++){
//LED green, scooting back
myFinch.setLED(0,255,0);
myFinch.setWheelVelocities(-255,0,(2 * time));

//Pause
myFinch.sleep(2 * time);

//LED yellow, scootint back
myFinch.setLED(255,255,0);
myFinch.setWheelVelocities(0,-255,(2 * time));
}
}

public static void chorus(){

//Initializing needed info
Finch myFinch = new Finch();
int time = 400;

for(int h = 0; h < 2; h++){
myFinch.sleep(time);
for(int i = 0; i < 5; i++){
//forward, back, forward, back (yellow, green, yellow, green)
myFinch.setLED(255,255,0);
myFinch.setWheelVelocities(180,180,time);
myFinch.setLED(0,255,0);
myFinch.setWheelVelocities(-180,-180,time);
}
myFinch.sleep(2 * time);
for(int i = 0; i < 2; i++){
//LED to red, wag "tail"
myFinch.setLED(255,0,0);
myFinch.setWheelVelocities(-255,255,(time));
myFinch.setWheelVelocities(255,-255,(time));

//Pause
myFinch.sleep(2 / time);

//LED to blue, wag "tail"
myFinch.setLED(0,0,255);
myFinch.setWheelVelocities(-255,255,(time));
myFinch.setWheelVelocities(255,-255,(time));
}
}
}

public static void verse(){

//Initializing needed info
Finch myFinch = new Finch();
int time = 400;

for(int i = 0; i < 2; i++){
//LED to red, move foward, turn left, turn right
myFinch.setLED(255,0,0);
  myFinch.setWheelVelocities(90,90,(2 * time));
myFinch.setWheelVelocities(-180,180,time);
myFinch.setWheelVelocities(180,-180,time);

//LED to blue, move backword, turn right, turn left
myFinch.setLED(0,0,255);
myFinch.setWheelVelocities(-180,-180,(2 * time));
myFinch.setWheelVelocities(180,-180,time);
myFinch.setWheelVelocities(-180,180,time);

//LED to red, move foward, turn left, turn right
myFinch.setLED(255,0,0);
  myFinch.setWheelVelocities(90,90,(2 * time));
myFinch.setWheelVelocities(-180,180,time);
myFinch.setWheelVelocities(180,-180,time);
}
//Pause
myFinch.sleep(2 * time);
//LED green, spin around
myFinch.setLED(0,255,0);
myFinch.setWheelVelocities(-255,255,(10 * time));

//Pause
myFinch.sleep(2 * time);
//LED yellow, spin around
myFinch.setLED(255,255,0);
myFinch.setWheelVelocities(255,-255,(10 * time));

//facing forward
myFinch.setLED(0,255,0);
myFinch.setWheelVelocities(-255,255,(600));
}
}
